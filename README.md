# Inline NASM #

Just a quick proof of concept to have inline assembly in Python. Provides a single function (`asm`) that, given a [nasm](http://www.nasm.us/)-compatible assembly block, assembles it, loads it into in executable memory and returns a ctype function, ready to be called.

## Requirements ##

- Python 2.7;
- a recent enough version of nasm;
- Linux (should probably work on OS X with minor modifications);
- the example is written in x86\_64 assembly, so a 64 bit machine is required for it.